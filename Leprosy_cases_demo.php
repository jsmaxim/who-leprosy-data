<?php

class Leprosy_cases_demo
{
    /**
     * See https://apps.who.int/gho/data/view.main.95300 table
     */
    const LEPROSY_CASES_URL = 'https://ghoapi.azureedge.net/api/WHS3_45';
    const COUNTRY_CODES_URL = 'https://apps.who.int/gho/athena/data/COUNTRY.json';
    const CONST_HTTP_OK_RESPONSE = 200;

    /**
     * class constructor
     */
    public function __construct()
    {
        $leprosy_cases_data = $this->getData(self::LEPROSY_CASES_URL);
        $country_codes_data = $this->getData(self::COUNTRY_CODES_URL);

        //leprosy cases data
        $lc_data = $this->parseLeprosyData($leprosy_cases_data);
        //country_codes_data
        $cc_data = $this->parseCountryCodeData($country_codes_data);

        //merged data for the results table
        $table_data = $this->generateTable($lc_data, $cc_data);

        //include front-end
        include('Leprosy_table.phtml');
    }

    /**
     * @param string $url
     * @return string
     */
    private function getData(string $url): string
    {
        $guzzleClient = new \GuzzleHttp\Client();
        $res = $guzzleClient->get($url);
        if ($res->getStatusCode() !== self::CONST_HTTP_OK_RESPONSE) {
            echo 'Could not retrieve WHO data.';
            exit(0);
        }
        return $res->getBody()->getContents();
    }

    /**
     * @param string $content
     * @return array
     */
    private function parseLeprosyData(string $content): array
    {
        return array_map(function ($obj) {
            return $obj;
        }, json_decode($content)->value);
    }

    /**
     * @param string $content
     * @return array
     */
    private function parseCountryCodeData(string $content): array
    {
        $content = reset(json_decode($content)->dimension);
        return array_map(function ($obj) {
            return [
                'label' => $obj->label,
                'name' => $obj->display
            ];
        }, ($content)->code);
    }

    /**
     * @param array $leprosy_cases_data
     * @param array $country_codes_data
     * @return array
     */
    private function generateTable(array $leprosy_cases_data, array $country_codes_data): array
    {
        $res = [];

        //loop through leprosy data and country codes
        foreach ($leprosy_cases_data as $leprosy_cases_obj) {
            $leprosy_country_label = $leprosy_cases_obj->SpatialDim;

            foreach ($country_codes_data as $country_codes_row) {
                $label = $country_codes_row['label'];

                //check for a match
                if ($label === $leprosy_country_label) {

                    //set required values
                    $country_name = $country_codes_row['name'];
                    $year = $leprosy_cases_obj->TimeDim;
                    $value = $leprosy_cases_obj->Value;
                    if (!is_numeric($value)) {
                        $value = null;
                    }

                    //fill result
                    $res[$country_name][$year] = $this->formatValues($value);
                }
            }
        }
        return $res;
    }

    public function formatValues(?string $value): string
    {
        return number_format($value, 2, '.', '');
    }
}